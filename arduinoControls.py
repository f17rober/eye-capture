from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
import time
import os
import cv2 #Pour les outils d'enregistrement d'images



from UUTrack.Model.Cameras.Hamamatsu import camera

from workerThread import WorkThread

class ArduinoControls():
    """
    Outils de contrôle de la carte Arduino
    """





    def __init__(self):
        super().__init__()



    def changeLedStatus(self, port):
        port.write(bytes('1', 'UTF-8'))

    def connectArduino(self, port_number):
        try:
            self.port = Serial(port_number, 9600)
            if self.port.isOpen():
                self.port.close()
            self.port.open()
            print("Port ouvert")
        except serial.serialutil.SerialException:
            print("Impossible d\'ouvrir le port série, il est déjà utilisé")
