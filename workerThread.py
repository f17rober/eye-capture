from PyQt5.QtCore import QThread, pyqtSignal


class WorkThread(QThread):
    """
    Thread séparé permettant d'éviter le freeze de la fenêtre lors de l'acquisition
    """
    image = pyqtSignal(object)  # Déclaration d'un signal personnalisé 'image' que le workthread envoie lorsqu'une
    # image est acquise
    thread_ending = pyqtSignal()

    def __init__(self, camera, port, exposure_times, parent=None):
        super().__init__(parent)
        self.camera = camera
        self.port = port
        self.exposure_times = exposure_times
        self.keep_acquiring = True
        self.origin = None  # Variable servant à dire si on veut juste acquérir une frame, utilisée dans la fctn 'snap'
        self.numberFramesToAcquire = None  # Nombre de frames à acquérir

    def __del__(self):
        self.wait()

    def run(self):
        """
        Appelé par la méthode spéciale '.start()'
        """
        first = True
        n = 1

        print(self.numberFramesToAcquire)

        while self.keep_acquiring:

            if self.origin == 'snap':
                self.keep_acquiring = False  # Si on ne veut acquérir qu'une seule frame (fctn 'snap'), on arrête
                # l'acquisition dès la fin du premier tour de boucle
                if n < self.numberFramesToAcquire:
                    self.keep_acquiring = True
                    n += 1

            if first:
                self.camera.setAcquisitionMode(
                    self.camera.MODE_CONTINUOUS)  # Lors de l'appel de cette méthode, on passe la caméra en mode
                # "acquisition continue"
                self.camera.triggerCamera()
                first = False
            self.port.write(bytes(str(n - 1), 'UTF-8'))
            self.camera.setExposure(self.exposure_times[n])
            img = self.camera.readCamera()
            self.image.emit(img)  # Emission du signal 'image' pour indiquer que des données sont reçues
        self.thread_ending.emit()
        self.camera.stopAcq()  # Arrêt de l'acquisition
        return
