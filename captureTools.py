from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import time
import os
import cv2 #Pour les outils d'enregistrement d'images
import numpy as np



from UUTrack.Model.Cameras.Hamamatsu import camera

from workerThread import WorkThread

class CaptureToolsWindow(QMainWindow):
    """
    Fenêtre contenant les outils permettant de contrôler la capture (start-stop) et l'acquisition
    """

    start_camera = pyqtSignal()
    start_acqu = pyqtSignal()
    start_snap = pyqtSignal()
    send_image = pyqtSignal(object)


    
    def __init__(self, camera):
        super().__init__()

        self.camera = camera  # On lie l'attribut "camera" à la classe StartWindow
        self.camera.initializeCamera()  # Initialisation de la caméra
        self.port = ''
        self.exposure_times = [33, 33, 33, 33, 33, 33, 33, 33, 33]

        #self.exposure_times = [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000]


        self.central_widget = QWidget()

        self.buttonStartStopLive = QPushButton('Lancer/Stopper la vue en direct', self.central_widget)
        
        self.infoStatusLive = QWidget(self.central_widget)
        self.statusLive = QLabel('Désactivée', self.infoStatusLive)
        
        self.buttonStartAcqu = QPushButton('Lancer l\'acquisition', self.central_widget)

        self.exposureParameters = QWidget(self.central_widget)
        self.exposureSlider = QSlider(Qt.Horizontal, self.exposureParameters)
        self.exposureSlider.setMinimum(4)
        self.exposureSlider.setMaximum(100)
        self.exposureSlider.setValue(self.camera.getExposure())
        self.exposureSlider.setSingleStep(1)
        self.exposureSlider.setTickInterval(10)
        self.exposureSlider.setTickPosition(QSlider.TicksBelow)
        self.exposureSpinBox = QSpinBox(self.exposureParameters)
        self.exposureSpinBox.setMinimum(4)
        self.exposureSpinBox.setMaximum(100)
        self.exposureSpinBox.setValue(self.camera.getExposure())

        self.binningParameters = QWidget(self.central_widget)
        self.binningComboBox = QComboBox(self.binningParameters)
        self.binningComboBox.addItem('1x1')
        self.binningComboBox.addItem('2x2')
        self.binningComboBox.addItem('4x4')

        self.binningComboBox.setCurrentText(str(self.camera.getBinning()))

        self.layout_infos = QHBoxLayout(self.infoStatusLive)
        self.layout_infos.addWidget(QLabel('Statut de la vue en direct:', self.infoStatusLive))
        self.layout_infos.addWidget(self.statusLive)
        self.layout_infos.addStretch()

        self.layout_exposure = QHBoxLayout(self.exposureParameters)
        self.layout_exposure.addWidget(self.exposureSlider)
        self.layout_exposure.addWidget(self.exposureSpinBox)
        self.layout_exposure.addWidget(QLabel('ms', self.exposureParameters))

        self.layout_binning = QHBoxLayout(self.binningParameters)
        self.layout_binning.addWidget(QLabel('Binning:', self.binningParameters))
        self.layout_binning.addWidget(self.binningComboBox)
        
        
        self.layout = QVBoxLayout(self.central_widget)
        #self.layout.addStretch()
        self.layout.addWidget(self.buttonStartStopLive)
        self.layout.addWidget(self.infoStatusLive)
        self.layout.addWidget(self.buttonStartAcqu)
        #self.layout.addStretch()
        self.layout.addWidget(QLabel('Temps d\'exposition:', self.central_widget))
        self.layout.addWidget(self.exposureParameters)
        self.layout.addWidget(self.binningParameters)
        self.layout.addStretch()
        
        self.setCentralWidget(self.central_widget)



        self.buttonStartStopLive.clicked.connect(self.start_camera.emit)
        self.buttonStartAcqu.clicked.connect(self.start_snap.emit)
        self.exposureSlider.valueChanged.connect(self.exposureSpinBox.setValue)
        self.exposureSpinBox.valueChanged.connect(self.exposureSlider.setValue)
        self.exposureSlider.valueChanged.connect(self.camera.setExposure)
        self.exposureSpinBox.valueChanged.connect(self.camera.setExposure)



        self.binningComboBox.activated[str].connect(self.setBinning)
        


        self.acquiring = False  #Par défaut, la caméra n'acquiert rien

        self.refreshTimer = QTimer()  #Déclaration d'un timer pour le rafraîchissement de l'image
        self.refreshTimer.timeout.connect(self.updateGUI)  #On rafraîchit l'image à chaque fois que le timer et écoulé
        self.refreshTimer.start(100)  #Réglage de la période de rafraîchissement: toutes les 100 ms

        self.numberFramesToAcquire = 8  #Indique le nombre d'images qu'on veut entregistrer par acquisition
        self.numberFramesAcquired = 0
        self.tempImage = []
        #self.savingDir = 'C:/'
        self.savingDir = os.path.expanduser('~')
        self.patientName = 'default_patient'
        self.statusLive.setText('Desactivée')





    def startCamera(self):
        """
        Démarre acquisition continue si elle est arrêtée ou l'arrête si elle fonctionne
        """
        if self.acquiring:
            self.stopCamera()

        else:
            self.acquiring = True
            self.statusLive.setText('Activée')
            self.workerThread = WorkThread(self.camera, self.port, self.exposure_times) #Définition du workerthread
            self.workerThread.image.connect(self.getData) #On récupère les données quand le workTread nous dit qu'il y a une image en nous envoyant le signal 'image'
            self.workerThread.start() #Démarrage du workerthread
            self.buttonStartAcqu.setEnabled(False)
            self.binningComboBox.setEnabled(False)
            self.workerThread.thread_ending.connect(self.enableStartAcquButton)

            
    def stopCamera(self):
        """
        Stoppe l'acquisition continue

        Si une acquisition est en cours, on dit au workerThread de ne pas la poursuivre et on passe l'état de la caméra en 'pas d'acquisition'
        """
        if self.acquiring:
            self.workerThread.keep_acquiring = False
            self.acquiring = False
            self.statusLive.setText('Desactivée')


    def snap(self):
        """
        Acquiert un nombre self.numberFramesToAcquire de frames de la caméra
        """


        if self.acquiring:
            msgBox = QMessageBox.critical(self, 'Vue en direct activée...', 'Pour commencer l\'acquisition, veuillez d\'abord désactiver la vue en direct.', QMessageBox.Ok)
            
            
        else:
            self.workerThread = WorkThread(self.camera, self.port, self.exposure_times)
            self.workerThread.image.connect(self.getData)
            self.workerThread.origin = 'snap' #On dit au workerthread que l'on veut juste acquérir 1 frame
            self.workerThread.numberFramesToAcquire = self.numberFramesToAcquire
            self.workerThread.start()
            self.buttonStartAcqu.setEnabled(False)
            self.workerThread.thread_ending.connect(self.enableStartAcquButton)
            self.numberFramesAcquired = 0



    def enableStartAcquButton(self):
        """
        Rend le bouton de lancement de l'acquisition cliquable
        """
        self.buttonStartAcqu.setEnabled(True)
        self.binningComboBox.setEnabled(True)
        

    def getData(self, data):
        """
        Récupère les données collectées par le workThread
        Sauvegarde les données si on lance une acquisition
        """
        if len(data) >= 1:
            self.tempImage = data[0] #Récupération des données si elles existent et stockage dans 'tempImage'
            if self.workerThread.origin == 'snap': # Si on a lancé une acquisition

                self.numberFramesAcquired += 1
                whole_dir = self.savingDir + '/' + self.patientName + '0' + str(self.numberFramesAcquired) + '.png'
                whole_dir.replace('\'', '')
                self.tempImageNormalized = cv2.normalize(self.tempImage, None, alpha=0, beta=255, norm_type = cv2.NORM_MINMAX, dtype = cv2.CV_32F) #Normalizes the image with a Min-max norm  # To convert in an 8 bits format
                self.tempImageNormalized_utf8 = cv2.convertScaleAbs(self.tempImageNormalized, alpha=(
                    255.0))  # Converts the values on the scale 0-255 without overflow
                self.tempImageNormalized_utf8 = np.uint8(self.tempImageNormalized_utf8)  # Convert in an 8 bits numpy array
                cv2.imwrite(whole_dir, self.tempImageNormalized_utf8, [cv2.IMWRITE_PNG_COMPRESSION,
                                                                    3])  # Saves the image. Sets the png compression to 3 (default value)


    def rotate_image(self, img):
        """
        Permet la rotation de l'image acquise par la caméra pour qu'elle soit affichée dans le bon sens
        """
        (width, height) = img.shape[:2]
        center = (width/2, height/2)
        angle = 90 #Angle in degrees
        scale = 1 #Scale of the transformation
        rotation = cv2.getRotationMatrix2D(center, angle, scale)
        return cv2.warpAffine(img, rotation, (height, width))

    def updateGUI(self):
        """
        Met à jour l'affichage de l'image à intervalles réguliers, définis par le timer

        Si une image est stockée et que le signal 'image' est lancé par le workerThread, on actualise l'affichage
        """
        if len(self.tempImage) >= 1:
            self.send_image.emit(self.rotate_image(self.tempImage)) #Rotated image

    def getAcquisitionStatus(self):
        if self.acquiring:
            self.statusLive.setText('Activée')
        else:
            self.statusLive.setText('Désactivée')



    def setExposure(self, exposure):
        """
        Règle le temps d'exposition de la caméra (en ms)
        """
        if exposure < 3:
            self.camera.setExposure(3)
        elif exposure > 10000:
            self.camera.setExposure(10000)
        else:
            self.camera.getExposure(exposure)
        print(self.camera.getExposure)

###TO UNCOMMENT LATER
    def setBinning(self, binning):
       """
       Règle le binning de la caméra (1x1, 2x2 ou 4x4)
       """
       if (binning=="1x1") or (binning=="2x2") or (binning=="4x4"):
           self.camera.setBinning(int(binning[0]))
       else:
           pass
        





if __name__ == '__main__':
    app = QApplication([])
    cam = camera(0)
    window = CaptureToolsWindow(cam)
    window.show()
    app.exit(app.exec_())
