//Programme de pilotage de l'illuminant V2

#include <Wire.h>
#include <DS1844.h>


int brightness[5];  //This creates a 4 element array for the brightness values of the 4 LEDS
int readout;               //This is the DS1844's returned value
const int address = 0x28;  //This is the address of the DS1844 chip with all three address pins grounded.
char p='n';

//Create an instance of the DS1844 class
DS1844 ds1844(address);

void setup() 
{
  //Start serial communication
  Serial.begin(9600);
}

void loop() 
{
  
  //Contrôl manuel de la valeur des 4 potentiomètres
  /*******************************************************************************************************************************************************************************************/

  if(Serial.available() > 0)
  {
    p=Serial.read();

    
    if ((p=='n') || (p == '0'))
    {
      brightness [0]=0;  // Bleu
      brightness [1]=0;  // Magenta
      brightness [2]=0;  // Cyan
      brightness [3]=0;  // Orange
    }

    
    if (p=='1')
    {
      brightness [0]=63; // Bleu
      brightness [1]=0;  // Magenta
      brightness [2]=0;  // Cyan
      brightness [3]=0;  // Orange
    }
    else if(p=='2')
    {
      brightness [0]=0;  // Bleu
      brightness [1]=63; // Mangenta
      brightness [2]=0;  // Cyan
      brightness [3]=0;  // Orange
    }
    else if(p=='3')
    {
      brightness [0]=0;  // Bleu
      brightness [1]=0;  // Magenta
      brightness [2]=63; // Cyan
      brightness [3]=0;  // Orange
    }
    else if(p=='4')
    {
      brightness [0]=0;  // Bleu
      brightness [1]=0;  // Magenta
      brightness [2]=0;  // Cyan
      brightness [3]=63; // Orange
    }

    
    for (int i = 0; i <= 3; i++) 
    {
      ds1844.write(i, brightness[i]);   //Write the brightness to the selected potentiometer
      readout = ds1844.read(i);         //Display the newly written value to the serial monitor
      Serial.println(readout);
      delay(50);
    }
  }
  //p= -1;
  //delay(1000);


  /*******************************************************************************************************************************************************************************************/
  //Balaye de toutes les valeurs du potentiomètre pour effectué calibrage au spectro
  /*******************************************************************************************************************************************************************************************/
/*
  brightness [0]=63;
  brightness [1]=63;
  brightness [2]=63;
  brightness [3]=0;

ds1844.write(0, brightness[0]);
ds1844.write(1, brightness[1]);
ds1844.write(2, brightness[2]);

  //Increment through the potentiometers

  //NOTE: DS1844 RANGE IS 0-63 (6 bit integer) 
  
    while (brightness[3] <63) 
    {
      brightness[3]++;                  //Increment brightness
      ds1844.write(3, brightness[3]);   //Write the brightness to the selected potentiometer
      readout = ds1844.read(3);         //Display the newly written value to the serial monitor
      Serial.println(readout);
      delay(1000);
    }*/
 
/*  //Turn all pots back to zero
  for (int i = 0; i <= 3; i++) {
      ds1844.write(i, 0);
      brightness[i] = 0;}*/
  

  /*******************************************************************************************************************************************************************************************/
  //Contrôle de la valeur des potentiomètre par une liaison série
  /*******************************************************************************************************************************************************************************************/
  /*
  if (Serial.available()) //s'il y a des données qui arrivent
  {
    nombre = Serial.parseInt();//Lecture d'un entier sur le tampon série
  }
  
  Serial.flush(); //On vide le tampon
  */
  /*******************************************************************************************************************************************************************************************/

  //Wait a second
 //delay(70000);


  

}
