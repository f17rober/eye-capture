"""
    patientName.py
    ==============
   Widget gérant la sélection du nom du patient et du dossier d'enregistrement des images acquises
"""

# -*- coding: utf-8 -*

from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
from PyQt5.QtWidgets import *
import os #Pour la sélection du répertoire 'home'
from unidecode import unidecode #Pour transformer le nom du patient en chaine ASCII et éviter noms de fichiers aberrants

class PatientNameWindow(QMainWindow):
    """
    Permet de rentrer le nom du patient et le dossier d'enregistrement
    Gère l'enregistrement des captures
    """

    patient_name = pyqtSignal(object) #Signal émettant le nom du patient (encodage ASCII)
    saving_dir = pyqtSignal(object)  #Signal émettant le chemin d'enregistrement des images acquises
        
    def __init__(self):
        super().__init__()
        self.central_widget = QWidget()

        self.inputPatientName = QLineEdit(self.central_widget)
        self.lblPatientName = QLabel('', self.central_widget)

        self.dirSavingButton = QPushButton('Dossier d\'enregistrement', self.central_widget)
        self.dirSaving = os.path.expanduser('~') #Le dossier d'enregistrement par défaut est le dossier personnel de l'utilisateur courant
        self.lblDirSaving = QLabel(self.dirSaving, self.central_widget)

        self.layout = QVBoxLayout(self.central_widget)

        
        self.layout.addWidget(QLabel('Nom du patient:', self.central_widget))
        self.layout.addWidget(self.inputPatientName)
        self.layout.addWidget(self.lblPatientName)
        self.layout.addWidget(self.dirSavingButton)
        self.layout.addWidget(QLabel('Dossier d\'enregistrement:', self.central_widget))
        self.layout.addWidget(self.lblDirSaving)
        self.layout.addStretch()

        self.setCentralWidget(self.central_widget)

        self.inputPatientName.textChanged.connect(self.lblPatientName.setText)
        self.inputPatientName.textChanged.connect(self.new_patient)
        self.dirSavingButton.clicked.connect(self.set_saving_directory)



    def new_patient(self, name):
        """
        Emet le nom du patient pour donner un nom aux fichiers enregistrés
        """
        self.patient_name.emit(unidecode(name))


    def set_saving_directory(self):
        """
        Règle le dossier d'enregistrement des images acquises
        """
        previous_dirSaving = self.dirSaving #Sauvegarde du chemin de dossier précédent la sélection d'un nouveau chemin
        self.dirSaving = QFileDialog.getExistingDirectory(None, 'Select a folder:', self.dirSaving, QFileDialog.ShowDirsOnly) #Fenêtre de sélection de dossier
        if self.dirSaving == '': #Décrit le cas où l'on annule la fenêtre de sélection de fichier
            self.dirSaving = previous_dirSaving #On remet l'ancien chemin comme chemin choisi
            
        self.lblDirSaving.setText(self.dirSaving) #Affichage du chemin d'enregistrement
        self.saving_dir.emit(self.dirSaving) #Emission du chemin d'enregistrement








if __name__ == '__main__':
    app = QApplication([])

    window = PatientNameWindow()
    window.show()
    app.exit(app.exec_())
