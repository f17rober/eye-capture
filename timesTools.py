from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
from PyQt5.QtWidgets import *

class TimeTable(QMainWindow):
    """
    
    """

    new_exposure_times = pyqtSignal(object)
    
    def __init__(self):
        super().__init__()
        
        self.wlengths_table = QTableWidget()
        self.wlengths_table.sizeHint()

        ###Paramètres pouvant être modifiés
        self.nb_wlengths = 7
        self.colors = ['Blanc', 'Bleu', 'Magenta', 'Cyan', 'Orange', 'Jaune?', 'Rouge?']
        self.exposure_times = [33, 33, 33, 33, 33, 33, 33]
        ###

        self.wlengths_table.setRowCount(self.nb_wlengths)
        self.wlengths_table.setColumnCount(2)
        self.temp_row = 0
        self.temp_column = 0
        self.temp_value = 0
        self.wlengths_table.setHorizontalHeaderLabels(['Couleur', 'Temps d\'exposition (en ms)'])
        self.wlengths_table.verticalHeader().setVisible(False) #Cache le compteur de lignes
        self.horizontal_header = self.wlengths_table.horizontalHeader()
        self.horizontal_header.setSectionResizeMode(0, QHeaderView.ResizeToContents)#Redimensionnement des colonnes pour que leur contenu ne soit pas coupés au démarrage du programme
        self.horizontal_header.setSectionResizeMode(1, QHeaderView.ResizeToContents)

        

        for n in range (0, len(self.colors)):
            cell_color = self.wlengths_table.cellWidget(n,1)
            widget_color = QTableWidgetItem(self.colors[n])
            widget_color.setFlags(Qt.ItemIsEnabled) #Empèche de modifier le nom de la couleur
            self.wlengths_table.setItem(n,0, widget_color)
            self.wlengths_table.setItem(n, 1, QTableWidgetItem(str(self.exposure_times[n])))

            




        #Ajout des widgets
        
        self.setCentralWidget(self.wlengths_table)
        
        self.wlengths_table.clicked.connect(self.change_active_cell)
        self.wlengths_table.cellChanged.connect(self.change_time_value)


        #Ajout des méthodes


    def change_active_cell(self):
        for currentQTableWidgetItem in self.wlengths_table.selectedItems():
            self.temp_row = currentQTableWidgetItem.row()
            self.temp_column = currentQTableWidgetItem.column()
            self.temp_value = currentQTableWidgetItem.text()
            

    def change_time_value(self):
        new_value = self.wlengths_table.currentItem()
        #print(new_value.text())
        if (new_value.text().isdigit()) and (int(new_value.text()) >= 4) and (int(new_value.text()) <= 1000):
            self.temp_value = new_value
            self.exposure_times[self.wlengths_table.selectedItems()[0].row()] = int(new_value.text())
            print(self.exposure_times)
            self.new_exposure_times.emit(self.exposure_times)
        else:
            self.wlengths_table.setItem(self.temp_row, self.temp_column, QTableWidgetItem(str(self.temp_value)))
            
                





if __name__ == '__main__':
    app = QApplication([])

    window = TimeTable()
    window.show()
    app.exit(app.exec_())
