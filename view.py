"""
    view.py
    =======
    Widget principal, contrôlant tous les autres. À lancer en premier
"""

import numpy as np

from PyQt5.QtCore import Qt, QThread, QTimer, pyqtSignal
from PyQt5.QtWidgets import *
from pyqtgraph import ImageView

from patientName import PatientNameWindow
from captureTools import CaptureToolsWindow
from workerThread import WorkThread
from timesTools import TimeTable

from serial import Serial
import serial.serialutil
import serial.tools.list_ports

from UUTrack.Model.Cameras.Hamamatsu import camera  # Modèle permettant de contrôler la caméra


class StartWindow(QMainWindow):
    """
    Définition de la fenêtre principale et de ses méthodes
    """

    acquisition_status = pyqtSignal(object)

    def __init__(self, camera, screen_width, screen_height):

        super().__init__()  # On récupère les attributs par défaut de la classe QMainWindow
        self.camera = camera
        self.screen_width = screen_width
        self.screen_height = screen_height

        self.setWindowTitle('Eye Capture')  # Titre de la fenêtre

        self.central_widget = QWidget()  # On crée un widget principal qui contiendra les autres

        self.patientWindow = PatientNameWindow()  # Import des différents modules contenus dans les autres fichiers du programme
        self.captureWindow = CaptureToolsWindow(self.camera)
        self.timeWindow = TimeTable()
        self.imageView = ImageView()  # Cadre dans lequel sera affiché l'image
        self.imageView.setLevels(0, 2)
        self.buttonArduino = QPushButton('Test Leds')

        self.layout_pannel = QVBoxLayout()  # Layout vertical permettant d'organiser le widget latéral
        self.layout_pannel.addWidget(self.patientWindow)
        self.layout_pannel.addWidget(self.captureWindow)
        self.layout_pannel.addWidget(self.timeWindow)
        self.layout_pannel.addWidget(self.buttonArduino)
        self.layout_pannel.addStretch()

        self.layout = QHBoxLayout(self.central_widget)
        self.layout.addLayout(self.layout_pannel)
        self.layout.addStretch()
        self.layout.addWidget(self.imageView)
        self.imageView.setMinimumSize(int(0.80 * self.screen_width), int(
            0.75 * self.screen_height))  # Règlage de la taille minimum de la fenêtre de visualisation en fonction de la taille de l'écran

        self.setCentralWidget(self.central_widget)  # Le widget principal défini comme widget central

        ###On définit une barre d'état
        self.statusBar = QStatusBar()  # Création de la barre d'état

        self.listAvailableCOMPorts = QComboBox(self.statusBar)

        ports = list(serial.tools.list_ports.comports())  # Indique tous les ports série disponibles

        self.port = Serial(ports[0][0], 9600)  # Variable initialisée avec le premier port de la liste

        first = True
        for p in ports:
            self.listAvailableCOMPorts.addItem(p[0])  # On crée la liste des ports disponibles
            if first:
                self.connectArduino(p[0])  # Ouverture du premier port trouvé
                first = False

        self.command_led = '0'

        self.statusBar.addPermanentWidget(QLabel('Caméra connectée:'))
        self.statusBar.addPermanentWidget(
            QLabel(str(camera.getSerialNumber()).split('\'')[1]))  # Affichage du numéro de série de la caméra
        self.statusBar.addPermanentWidget(QLabel('Port de la carte Arduino:'))
        self.statusBar.addPermanentWidget(
            self.listAvailableCOMPorts)  # Liste permettant de choisir le port série à utiliser

        self.setStatusBar(self.statusBar)  # Insertion de la barre d'état dans la fenêtre
        ###

        ###Association des signaux
        self.captureWindow.start_camera.connect(self.captureWindow.startCamera)
        self.captureWindow.start_snap.connect(self.captureWindow.snap)
        self.buttonArduino.clicked.connect(self.changeLedStatus)
        ###

        self.tempImage = []  # Stockage des images temporaires

        self.captureWindow.send_image.connect(self.imageView.setImage)  # Affichage de la dernière frame reçue
        self.patientWindow.patient_name.connect(self.setPatientName)  # Réception du nom du patient
        self.patientWindow.saving_dir.connect(self.setSavingDir)  # Réception du répertoire d'enregistrement

        self.listAvailableCOMPorts.activated[str].connect(self.connectArduino)
        self.timeWindow.new_exposure_times.connect(self.updateExposureTimes)

    ###On écrit les méthodes

    def setPatientName(self, patient_name):
        """
        Recoit le nom du patient et l'émet vers la fenêtre d'outils de capture
        """
        self.captureWindow.patientName = patient_name
        # print(self.captureWindow.patientName)

    def setSavingDir(self, saving_dir):
        """
        Recoit le répertoire d'enregistrement et l'émet vers la fenêtre d'outils de capture
        """
        self.captureWindow.savingDir = saving_dir
        # print(self.captureWindow.savingDir)

    def connectArduino(self, port_number):
        self.port.close()
        try:
            self.port = Serial(port_number, 9600)
            if self.port.isOpen():
                self.port.close()
            self.port.open()
            print("Port " + port_number + " ouvert")
            self.captureWindow.port = self.port
        except serial.serialutil.SerialException:
            print("Impossible d\'ouvrir le port série, il est déjà utilisé")

    def updateExposureTimes(self, exposure_times):
        self.captureWindow.exposure_times = exposure_times

    def changeLedStatus(self):

        if int(self.command_led) == 8: #Nombre de LEDs
            self.command_led = '0'
        else:
            self.command_led = str(int(self.command_led) + 1)

        self.port.write(bytes(self.command_led, 'UTF-8'))


if __name__ == '__main__':
    app = QApplication([])
    screen_geometry = app.desktop().screenGeometry()
    screen_width, screen_height = screen_geometry.width(), screen_geometry.height()
    cam = camera(0)  # On choisit la caméra qu'on veut utiliser. Ici, la première trouvée

    window = StartWindow(cam, screen_width, screen_height)
    window.show()
    app.exit(app.exec_())
